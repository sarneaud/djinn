module djinn;

@safe:

import std.array;
import std.typecons : Flag, Yes, No;

import djinn.parsing;
import djinn.translation;
public import djinn.types;

/*
	High-level algorithm:
	1. Parse Djinn code into tokens
	2. Post-process tokens (for whitespace trimming)
	3. Translate tokens to D code
	[< raw >] is handled specially in step 1 because it affects parsing.  Other directives (including "*include") are handled in the translation step.
*/

/**
  Converts Djinn code to a D code string

	Params:
		src = Djinn code source
		fname = name of file containing Djinn code

	Returns: D code string
	Throws: SyntaxException, std.utf.UTFException, std.file.FileException
*/
string translate(Source* src)
{
	auto tokens = getTokens(src);
	auto output = appender!(string);
	translateTokens!(No.useMixins)(output, tokens);
	return output[];
}

/// ditto
string translate(string fname)()
{
	auto src = new Source(fname, import(fname));
	auto tokens = getTokens(src);
	auto output = appender!(string);
	translateTokens!(Yes.useMixins)(output, tokens);
	return output[];
}

void writeText(Writer, U...)(auto ref Writer w, U args)
{
	import std.conv : text;
	// FIXME: add a version of text() that supports ranges to Phobos
	w.put(text(args));
}
