module djinn.lexing;

// There's no separate lexing phase for lexing the top-level Djinn language
// This module is for sub-language lexing utilities

@safe:

import std.algorithm;
import std.conv;
import std.exception;
import std.range;
import std.traits;
import std.typecons;
import std.utf;

package:

/// Avoids autodecoding
alias string8b = typeof("".byCodeUnit);

/// Nesting lexical entities that normally must be balanced in D code
enum Nestable
{
	braces = 1 << 0, // { }
	parens = 1 << 1, // ( )
	brackets = 1 << 2, // [ ]
}
/// Bitflag for nestables
alias Nestables = BitFlags!Nestable;
/// All nesting entities
enum kAllNestables = reduce!"a|b"(Nestables.init, [EnumMembers!Nestable]);

/// Exception for D lexical errors
class LexicalException : Exception
{
	this(string msg, size_t offset, string file = __FILE__, size_t line = __LINE__, Throwable next = null) @nogc @safe pure nothrow
	{
		this.offset = offset;
		super(msg, file, line, next);
	}
	size_t offset;
}

/**
  Like std.algorithm.find, but skips over D comments, string literals, etc.

	Params:
		code = D code, followed by terminator, followed by arbitrary text
		terminator = marker of end of D code
		balance_what = bitflag of nestable entities that must be balanced
		orig_offset = byte offset of code in original source file (used for error messages)

	Returns: Portion of code that starts with terminator (throws if not found)

	Throws: LexicalException on lexically malformed D code, or UTFException on UTF8 decoding error
*/
string8b dSkipFind(string8b code, string8b terminator, Nestables balance_what = kAllNestables, size_t orig_offset = 0) pure
{
	size_t orig_len = code.length;
	size_t brace_balance = 0, paren_balance = 0, bracket_balance = 0, total_balance = 0;

	LexicalException lexicalException(string msg) pure
	{
		// FIXME: this calculated offset can be useless if we've read to the end of code
		return new LexicalException(msg, orig_offset + (orig_len - code.length));
	}

	void notFound(string expected) pure
	{
		string hint;
		if (brace_balance > 0) hint ~= "  Are curly braces balanced?";
		if (paren_balance > 0) hint ~= "  Are parentheses balanced?";
		if (bracket_balance > 0) hint ~= "  Are square brackets balanced?";
		throw lexicalException(text("no ", expected, " found.", hint));
	}

	// Scans to last byte of the next newline (or throws if not found)
	void toEOL() pure
	{
		// https://dlang.org/spec/lex.html#end_of_line
eolloop:
		for (; !code.empty; code.popFront())
		{
			switch (code.front)
			{
				case '\0':
				case '\x1a':
					enforce(code.length == 1, lexicalException(`invalid D EOF character in Djinn code: \x` ~ to!string(code.front+0, 16)));
					return;

				case '\r':
					if (code.length == 1) return;
					if (code[1] == '\n')
					{
						code.popFront();
					}
					return;

				case '\n':
					return;

				static assert("\u2028" == "\xe2\x80\xa8");
				static assert("\u2029" == "\xe2\x80\xa9");
				case '\xe2':
					enforce!UTFException(code.length >= 3, text("invalid UTF-8 at byte ", orig_offset + (orig_len - code.length)));
					if (code[1] == '\x80' && code[2].among('\xa8', '\xa9'))
					{
						code = code[2..$];
						return;
					}
					continue;

				default:
					break;
			}
		}
		// The D code must end with the terminator
		notFound(terminator.source);
	}

lexloop:
	for (; !code.empty; code.popFront())
	{
		if (total_balance == 0 && code.startsWith(terminator)) return code;

		// https://dlang.org/spec/lex.html

		// Comments
		if (code.front == '/')
		{
			if (code.length < 2) continue lexloop;
			code.popFront();
			const type = code.front;
			code.popFront();

			switch (type)
			{
				case '/':
					toEOL();
					break;

				case '*':
					code = code.find("*/");
					if (code.empty) notFound("*/");
					code.popFront();
					break;

				case '+':
					size_t balance = 1;
					for (; !code.empty; code.popFront())
					{
						if (code.startsWith("/+"))
						{
							code.popFront();
							balance++;
							continue;
						}
						if (code.startsWith("+/"))
						{
							code.popFront();
							balance--;
							if (balance == 0) continue lexloop;
							continue;
						}
					}
					notFound("+/");
					break;

				default:
					continue;
			}
			continue lexloop;
		}

		// Character literals
		if (code.front == '\'')
		{
			if (code.length > 3)
			{
				if (code[1] == '\\')
				{
					code = code[3..$].find("'");
					if (code.empty) notFound("'");
					continue lexloop;
				}
				else
				{
					if (code[2] == '\'')
					{
						code = code[2..$];
						continue lexloop;
					}
				}
			}
			throw lexicalException("malformed character literal");
		}

		// WYSIWYG strings
		if (code.front == '`')
		{
			if (code.length > 1)
			{
				code = code[1..$].find("`");
				if (!code.empty) continue lexloop;
			}
			notFound("`");
		}
		if (code.startsWith("r\"") || code.startsWith("x\""))
		{
			// x"..." strings are actually (deprecated) hex strings, but the logic is the same
			if (code.length > 2)
			{
				code = code[2..$].find("\"");
				if (!code.empty) continue lexloop;
			}
			notFound("\"");
		}

		// Double-quoted strings
		if (code.front == '"')
		{
			code.popFront();
			for (; !code.empty; code.popFront())
			{
				if (code.front == '\\')
				{
					code.popFront();
					if (code.empty) break;
				}
				else if (code.front == '"')
				{
					continue lexloop;
				}
			}
			notFound("\"");
		}

		// Delimited strings
		if (code.startsWith("q\""))
		{
			code.popFront();
			code.popFront();
			if (code.empty) notFound("\"");
			// Case 1 (nesting delimiters)
			void nestingDelimitedStr(char left, char right) pure
			{
				size_t balance = 0;
				for (; !code.empty; code.popFront())
				{
					if (code.front == left)
					{
						balance++;
					}
					else if (code.front == right)
					{
						enforce(balance > 0, lexicalException("nesting delimited string not balanced"));
						balance--;
						if (balance == 0)
						{
							code.popFront();
							if (code.empty || code.front != '"') break;
							return;
						}
					}
				}
				notFound(right ~ "\"");
			}
			if (code.front == '[')
			{
				nestingDelimitedStr('[', ']');
				continue lexloop;
			}
			if (code.front == '(')
			{
				nestingDelimitedStr('(', ')');
				continue lexloop;
			}
			if (code.front == '<')
			{
				nestingDelimitedStr('<', '>');
				continue lexloop;
			}
			if (code.front == '{')
			{
				nestingDelimitedStr('{', '}');
				continue lexloop;
			}
			import std.uni : isAlpha;
			auto code_s = code.source;
			const front = decodeFront(code_s);
			// Case 2 (id delimiter, a.k.a heredoc)
			if (front == '_' || isAlpha(front))
			{
				auto id_start = code;
				toEOL();
				code.popFront();
				auto id = stripNewline(id_start[0..id_start.length - code.length]);
				do
				{
					if (code.startsWith(id) && code.length > id.length && code[id.length] == '"')
					{
						code = code[id.length..$];
						continue lexloop;
					}
					toEOL();
					code.popFront();
				} while (!code.empty);
				notFound(id.source ~ '"');
			}
			// Case 3 (plain delimiter)
			code_s = code_s.find(front);
			if (!code_s.empty)
			{
				code_s.popFront();
				if (code_s[0] == '"')
				{
					code = code_s.byCodeUnit;
					continue lexloop;
				}
			}
			throw lexicalException("malformed delimited string");
		}

		// Token strings
		if (code.startsWith("q{"))
		{
			code.popFront();
			code.popFront();
			code = dSkipFind(code, "}".byCodeUnit, kAllNestables, orig_offset + (orig_len - code.length));
			continue lexloop;
		}

		// Special token sequences
		if (code.front == '#')
		{
			toEOL();
			continue lexloop;
		}

		void nestUp(ref size_t counter, Nestable type, string name)
		{
			if (balance_what & type)
			{
				total_balance++;
				counter++;
			}
		}
		void nestDown(ref size_t counter, Nestable type, string name)
		{
			if (balance_what & type)
			{
				enforce(counter > 0, lexicalException(name ~ " not balanced"));
				assert (total_balance > 0);
				total_balance--;
				counter--;
			}
		}

		// Misc. balancing
		// but hopefully this makes error messages more helpful
		switch (code.front)
		{
			case '{':
				nestUp(brace_balance, Nestable.braces, "curly braces");
				break;

			case '}':
				nestDown(brace_balance, Nestable.braces, "curly braces");
				break;

			case '(':
				nestUp(paren_balance, Nestable.parens, "parentheses");
				break;

			case ')':
				nestDown(paren_balance, Nestable.parens, "parentheses");
				break;

			case '[':
				nestUp(bracket_balance, Nestable.brackets, "square brackets");
				break;

			case ']':
				nestDown(bracket_balance, Nestable.brackets, "square brackets");
				break;

			default:
				break;
		}
	}

	notFound(terminator.source);
	assert (false);
}

unittest
{
	bool test(string code)
	{
		bool pass = true;
		foreach (terminator; [">]", "]", "}"])
		{
			auto combined = code ~ terminator;
			auto result = dSkipFind(combined.byCodeUnit, terminator.byCodeUnit);
			pass &= (result == terminator.byCodeUnit);
		}
		return pass;
	}

	// Basic tests

	// Simple
	assert(test(``));
	assert(test(q{5}));
	assert(test(q{{}}));
	assert(test(q{{{{}}}}));
	assert(test(q{{if(a){return x;}else{return y;}}}));
	assert(test(`[]`));
	assert(test(`[[]]`));
	assert(test(`[[[]][]]`));
	assert(test(`[>]`));
	// Comments
	assert(test(q{
				5 // }}
				, 4}));
	assert(test(q{
				5 // /*
				, 4}));
	assert(test("//\n"));
	assert(test("//\r"));
	assert(test("//\u2028"));
	assert(test("//\u2029"));
	assert(test(q{/*}}{{*/}));
	assert(test(q{/*/*/}));
	assert(test(q{/*//*/}));
	assert(test(q{/*/}}/*/}));
	assert(test(q{/+/++/}}}+/}));
	assert(test(q{/+/}+/}));
	// Character literals
	assert(test(q{'x'}));
	assert(test(q{'\n'}));
	assert(test(q{'\''}));
	assert(test(q{'\"'}));
	assert(test(q{'\\'}));
	assert(test(q{'\x00'}));
	assert(test(q{'\u0000'}));
	assert(test(q{'\&amp;'}));
	assert(test(q{'"'}));
	assert(test(q{text('"', "}}")}));
	// WYSIWYG strings
	assert(test(r"``"));
	assert(test(r"`>]`"));
	assert(test(q{r""}));
	assert(test(q{r">]"}));
	assert(test(`x""`));
	assert(test(`x"00"`));
	// Double-quoted strings
	assert(test(q{""}));
	assert(test(q{"x"}));
	assert(test(q{"\n"}));
	assert(test(q{"\""}));
	assert(test(q{"\\"}));
	// Delimited strings #1
	assert(test(q{q"[asdf]"}));
	assert(test(q{q"("asdf)"}));
	assert(test(q{q"<asdf>"}));
	assert(test(q{q"{asdf}"}));
	assert(test(q{q"<<>]>"}));
	assert(test(q{q"{{}{{}}}"}));
	assert(test(q{q"[[]>]"}));
	assert(test(q{q"[[>]]"}));
	// Delimited strings #2
	assert(test(q{
auto s = q"EOS
EOS"}));
	assert(test(q{
auto s = q"EOS
asdf
EOS"}));
	assert(test(q{
auto s = q"∑
asdf
∑"}));
	assert(test(q{
auto s = q"EOS

E
O
S"

EOS"}));
	assert(test(q{
auto s = q"EOS
>]
EOS"}));
	assert(test(q{
auto s = q"EOS
}}
EOS"}));
	// Delimited strings #3
	assert(test(q{q"||"}));
	assert(test(q{q"|asdf|"}));
	assert(test(q{q"|"|"}));
	assert(test(q{q"|>]|"}));
	// Token strings
	assert(test(q{q{}}));
	assert(test(q{q{5}}));
	assert(test(q{q{{return 5}}}));
	assert(test(q{q{q{q{}}}}));
	// Special token sequences
	assert(test(q{
# line 42 ">]"
auto 4;
}));
}

private:

/// Strips exactly one D newline from the end of the given string
string8b stripNewline(string8b s) pure
in
{
	assert (!s.empty);
	assert (s.endsWith("\r") || s.endsWith("\n") || s.endsWith("\u2028".byCodeUnit) || s.endsWith("\u2029".byCodeUnit));
}
do
{
	if (s.length >= 3 && s[$-1].among('\xa8', '\xa9'))
	{
		return s[0..$-3];
	}
	if (s[$-1] == '\n') s = s[0..$-1];
	if (!s.empty)
	{
		if (s[$-1] == '\r')
		{
			s = s[0..$-1];
		}
	}
	return s;
}

unittest
{
	bool test(string s, string t)
	{
		return stripNewline(s.byCodeUnit) == t.byCodeUnit;
	}

	assert (test("\n", ""));
	assert (test("\r", ""));
	assert (test("\r\n", ""));
	assert (test("\n\r", "\n"));
	assert (test("\u2028", ""));
	assert (test("\u2029", ""));
	assert (test("\r\u2028", "\r"));
	assert (test("\n\u2028", "\n"));
	assert (test("\u2028\r", "\u2028"));
	assert (test("\u2028\n", "\u2028"));
}
