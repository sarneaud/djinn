module djinn.types;

@safe:

import std.array;
import std.algorithm;
import std.exception;
import std.range;
import std.utf;

/// Avoids autodecoding
package alias string8b = typeof("".byCodeUnit);

/// Tracks position in source code
struct Pos
{
	const(Source)* src;
	size_t offset;

	size_t getLineNum() const pure
	{
		return src.getLineNum(offset);
	}
}

/// A source code string with information about it
struct Source
{
	this(string fname, string orig) pure
	{
		import std.string : lineSplitter;
		this.fname = fname;
		this.orig = rem = orig.byCodeUnit;
		// The byte offsets of the start/end of each line are used for calculating line numbers
		line_offsets = chain(only(0), lineSplitter!(Yes.keepTerminator)(orig).map!(l => l.length).cumulativeFold!"a+b").array;
	}

	bool empty() const pure
	{
		return rem.empty;
	}

	size_t length() const pure
	{
		return rem.length;
	}

	// Returns current 0-based byte offset into source
	size_t offset() const pure
	{
		return orig.length - rem.length;
	}

	Pos curPos() return const pure
	{
		return posAt(offset);
	}

	/// Converts 0-based byte offset to Pos
	Pos posAt(size_t o) return const pure
	{
		return Pos(&this, o);
	}

	// Maps 0-based byte offset to 1-based line number
	size_t getLineNum(size_t o = offset()) const pure
	{
		// FIXME: support amortised O(line_offsets.length/tokens.length) approach
		assert (!line_offsets.empty);
		auto lines_after = assumeSorted(line_offsets).upperBound(o);
		return line_offsets.length - lines_after.length;
	}

	const(string) fname;
	const(string8b) orig;

	package:
	string8b rem;
	const(size_t[]) line_offsets;
}

/// For Djinn syntax errors
class SyntaxException : Exception
{
	mixin basicExceptionCtors;
}

package:

enum TokenType
{
	text = 0,
	statements,
	expressions,
	directive,
}

/// Jinja2 supports using - to indicate that whitespace should be stripped from the given side of the token
enum WhitespaceStripping
{
	none = 0,
	left = 1,
	right = 2,
	both = left | right,
}

/// Pieces of Djinn source code
struct Token
{
	TokenType type;
	string value;
	Pos pos;
	WhitespaceStripping whitespace_stripping;
}

alias TokenSink = Appender!(Token[]);

SyntaxException syntaxException(string msg, const(Source)* src, string file = __FILE__, size_t line = __LINE__) pure
{
	const pos = src.curPos();
	return syntaxException(msg, pos, file, line);
}

SyntaxException syntaxException(string msg, ref const(Pos) pos, string file = __FILE__, size_t line = __LINE__) pure
{
	import std.format : format;
	return new SyntaxException(format("%s(%d): %s", pos.src.fname, pos.getLineNum(), msg), file, line);
}
