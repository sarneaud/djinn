module djinn.translation;

@safe:

import std.array;
import std.exception;
import std.typecons : Flag, Yes, No;

import djinn.parsing;
import djinn.types;

// Hopefully something like this will be in Phobos someday
import djinn : writeText;

package:

/**
  Takes an array of tokens and writes D code to output

  No.useMixins: handle includes eagerly at run time
  Yes.useMixins: write mixin statements/expressions to include files when the D code is used with a mixin
*/
void translateTokens(Flag!"useMixins" use_mixins = Yes.useMixins)(Appender!string output, const(Token)[] tokens)
{
	import std.algorithm : startsWith;
	import std.string : stripLeft;
	static struct Fragment
	{
		string text;
		Pos pos;
	}
	auto fragments_app = appender!(Fragment[]);
	void flushFragments()
	{
		auto fs = fragments_app[];
		if (!fs.empty)
		{
			// FIXME: group lines
			if (fs.length == 1)
			{
				writeLineSequence(output, fs[0].pos);
				output.writeText("output.writeText(", fs[0].text, ");\n");
			}
			else
			{
				output.put("output.writeText(\n");
				foreach (i, f; fs)
				{
					writeLineSequence(output, f.pos);
					output.writeText(f.text, i == fs.length-1 ? ");\n" : ",\n");
				}
			}
			fragments_app.clear();
		}
	}
	foreach (token; tokens)
	{
		with (TokenType) final switch (token.type)
		{
			case text:
				if (!token.value.empty) fragments_app.put(Fragment(doubleQuote(token.value), token.pos));
				break;

			case statements:
				flushFragments();
				if (!token.value.empty)
				{
					writeLineSequence(output, token.pos);
					output.put(token.value);
					output.put("\n");
				}
				break;

			case expressions:
				string value = token.value;
				if (value.stripLeft.startsWith(`"`)) value = `format(` ~ value ~ ')';
				fragments_app.put(Fragment(value, token.pos));
				break;

			case directive:
				flushFragments();
				handleDirective!use_mixins(output, token);
		}
	}
	flushFragments();
}

private:

/**
  Adds line markers for mapping generated code lines to Djinn source lines

  https://dlang.org/spec/lex.html#special-token-sequence
*/
void writeLineSequence(Appender!string output, ref const(Pos) pos)
{
	output.writeText("# line ", pos.getLineNum(), ` "`, pos.src.fname, "\"\n");
}

void handleDirective(Flag!"useMixins" use_mixins = Yes.useMixins)(Appender!string output, ref const(Token) token)
{
	import std.algorithm.iteration : filter, splitter;
	import std.file : readText;
	if (token.value == "EOF") return;

	auto args = token.value.splitter.filter!(e => !e.empty);  // TODO: support quoting
	enforce(!args.empty, syntaxException("Empty [< directive", token.pos));
	const cmd = args.front;
	args.popFront();

	void checkLength(size_t req_len)
	{
		import std.conv : text;
		import std.range.primitives : walkLength;
		const len = args.walkLength;
		enforce(len == req_len, syntaxException(text(req_len, " arguments needed for directive \"", cmd,  "\" (got ", len, ")"), token.pos));
	}

	string getFname()
	{
		return args.front.resolvePathFrom(token.pos.src.fname);
	}

	switch (cmd)
	{
		case "include":
			checkLength(1);
			const fname = getFname();
			static if (use_mixins)
			{
				output.writeText("mixin(translate!", doubleQuote(fname), ");\n");
			}
			else
			{
				const contents = readText(fname);
				auto inc_src = new Source(fname, contents);
				auto tokens = getTokens(inc_src);
				translateTokens!use_mixins(output, tokens);
			}
			break;

		case "rawinclude":
			checkLength(1);
			const fname = getFname();
			static if (use_mixins)
			{
				output.writeText("output.writeText(import(", doubleQuote(fname), "));\n");
			}
			else
			{
				const contents = readText(fname);
				output.writeText("output.writeText(", doubleQuote(contents), ");\n");
			}
			break;

		case "xlatinclude":
			checkLength(1);
			const fname = getFname();
			static if (use_mixins)
			{
				output.writeText("output.writeText(translate!", doubleQuote(fname), ");\n");
			}
			else
			{
				const contents = readText(fname);
				auto inc_src = new Source(fname, contents);
				auto tokens = getTokens(inc_src);
				output.put("output.put(q{\n");
				translateTokens!use_mixins(output, tokens);
				output.put("});\n");
			}
			break;

		case "raw":
		case "endraw":
			break;

		default:
			throw syntaxException("unrecognised [< directive: " ~ cmd, token.pos.src);
	}
}

/// Interprets a path relative to the path of the source it was found in
string resolvePathFrom(string target_path, string src_path) pure
{
	import std.path;
	if (src_path == "<STDIN>") return target_path;
	return buildNormalizedPath(dirName(src_path), target_path);
}

pure
unittest
{
	assert (resolvePathFrom("foo.dj", "<STDIN>") == "foo.dj");
	assert (resolvePathFrom("foo.dj", "bar.dj") == "foo.dj");
	assert (resolvePathFrom("foo.dj", "/x/bar.dj") == "/x/foo.dj");
	assert (resolvePathFrom("x/foo.dj", "bar.dj") == "x/foo.dj");
	assert (resolvePathFrom("x/foo.dj", "y/bar.dj") == "y/x/foo.dj");
}

/// Returns the D double-quoted string literal version of the input string
string doubleQuote(string s) pure
{
	import std.format : format;
	import std.range : only;
	return format("%(%s%)", only(s));
}

pure
unittest
{
	assert (doubleQuote(``) == `""`);
	assert (doubleQuote(`x`) == `"x"`);
	assert (doubleQuote(`"x"`) == `"\"x\""`);
}
