import std.stdio;
import djinn;

void main(string[] args)
{
	const name = args.length > 1 ? args[1] : "World";
	greet(name);
}

void greet(string name)
{
	auto output = stdout.lockingTextWriter();
	mixin(translate!"greeter.dj");
}
