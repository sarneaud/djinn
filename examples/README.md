# Djinn Examples

## `hello.txt`

A Hello World example showing the basic common features.

## `include_hello.md`

A simple example of using `include` and `rawinclude` directives.

## `greeter1.d` and `greeter2.d`

Two ways to embed a Djinn template in D code.

`greeter1.d` uses CTFE.  It's self-contained D, but CTFE can be slow.

`greeter2.d` is itself generated using the command line code generator using `xlatinclude`.  That's faster, but means running an external tool in the build process.

Either way, the result is a program that prints a hello message using a Djinn template.  By default it says "Hello" to "World", but that can be overridden by giving it a command line argument.

## `normaldist.csv`

A simple example of using Djinn to generate data in CSV format.

Here's how you can plot the output with [`ggplotd-cli`](https://gitlab.com/sarneaud/ggplotd-cli):
```bash
ggplotd -H -d , -t line normaldist.csv
```

## `mandelbrot.pgm`

Generates a Mandelbrot set fractal in the [text-based Netpbm image format](http://netpbm.sourceforge.net/doc/#formats).

Here's the result converted to a PNG:

<img src="mandelbrot.png">

## `inequality.lp`

Solves the following number puzzle as a mixed integer linear programming (MILP) problem:

<img src="inequality.svg">

The 5x5 grid needs to be filled with values 1-5, with the values in each row and column distinct.  The numbers in neighbouring cells need to obey the inequality constraints indicated by the greater-than signs.  One number is already set.

The Djinn code encodes the puzzle as an MILP in [CPLEX format](http://lpsolve.sourceforge.net/5.0/CPLEX-format.htm).  Here's how to get a solution with the [GNU Linear Programming Kit](https://www.gnu.org/software/glpk/):
```bash
glpsol --lp inequality.lp -o /dev/stdout | awk '/v[0-9][0-9]/ { print $2, $4 }' | sort
```

Here's the solution, visually:

<img src="inequality_solution.svg">
