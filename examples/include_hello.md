# Including Hello World Example

`hello.text.dj` source:
```
Hello [= retro("dlrow") ]!
[: enum one = 1; :]
1 + 1 = [= one + one ]
```

Output:
```
Hello world!
1 + 1 = 2
```
